= Red Hat Process Automation Manager install demo 
 Eric D. Schabell @eschabell
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


== CodeReady Containers with Red Hat Process Automation Manager Install

Open the diagrams in the diagram tooling using 'Load Diagram' link. To download the project file for these diagrams use
the 'Download Diagram' link. The images below can be used to browse the available diagrams and can be embedded into your
content.

Use case: Getting started installing process automation product.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/crc-rhpam-install-demo.drawio[[Load Diagram]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/product-demos/crc-rhpam-install-demo.drawio?inline=false[[Download Diagram]]
--

--
image:product-demo-diagrams/crc-rhpam-install.png[350, 300]
--

