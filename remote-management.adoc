= Remote server management
Eric D. Schabell @eschabell, Iain Boyle @iainboy
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


This architecture covers remote server management. As more and more enterprises are deployed across different data
centers and public and/or private clouds, it is important to provide consistency and security in an automated way across all of the
the servers in order to reduce risk and costs. 

Use case: Providing remote management for a consist server estate across hybrid cloud and data centers with remote management,
security, and  data protection for full lifecycle.

Open the diagrams below directly in the diagram tooling using 'Load Diagram' link. To download the project file for these diagrams use
the 'Download Diagram' link. The images below can be used to browse the available diagrams and can be embedded into your content.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/remote-server-management.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/remote-server-management.drawio?inline=false[[Download Diagram]]
--

--
image:intro-marketectures/rsm-marketing-slide.png[750,700]
--

--
image:logical-diagrams/rsm-ld.png[350, 300]
image:schematic-diagrams/rsm-network-sd.png[350, 300]
image:schematic-diagrams/rsm-data-sd.png[350, 300]
--

--
image:detail-diagrams/rsm-smart-management.png[250, 200]
image:detail-diagrams/rsm-automation.png[250, 200]
image:detail-diagrams/rsm-image-store.png[250, 200]
image:detail-diagrams/rsm-scm.png[250, 200]
--

